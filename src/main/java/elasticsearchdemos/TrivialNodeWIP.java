package elasticsearchdemos;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;

import java.io.IOException;
import java.util.Date;

public class TrivialNodeWIP {

    public static void main(String[] argsIgnored) throws IOException {

        Settings settings = Settings.settingsBuilder()
                .put("path.home", "/Applications/elasticsearch-2.0.0/")
                .put("cluster.name", "epsteinj-elasticsearch-local")
                .put("http.enabled", true)
                .put("index.number_of_shards", 1)
                .put("index.number_of_replicas", 1).build();

        try (Node node = NodeBuilder.nodeBuilder().settings(settings).client(true).node()) {
            Client client = node.client();

            BulkRequestBuilder bulkRequest = client.prepareBulk();

// either use client#prepare, or use Requests# to directly build index/delete requests
            bulkRequest.add(client.prepareIndex("twitter", "tweet", "1")
                                    .setSource(jsonBuilder()
                                                       .startObject()
                                                       .field("user", "kimchy")
                                                       .field("postDate", new Date())
                                                       .field("message", "trying out Elasticsearch")
                                                       .endObject()
                                    )
            );

            bulkRequest.add(client.prepareIndex("twitter", "tweet", "2")
                                    .setSource(jsonBuilder()
                                                       .startObject()
                                                       .field("user", "kimchy")
                                                       .field("postDate", new Date())
                                                       .field("message", "another post")
                                                       .endObject()
                                    )
            );

            BulkResponse bulkResponse = bulkRequest.get();
            if (bulkResponse.hasFailures()) {
                // process failures by iterating through each bulk response item
                bulkResponse.forEach(response -> System.out.println(response.getFailureMessage()));
            } else {
                bulkResponse.forEach(response ->
                    System.out.println("id=" + response.getId() + ", item-id=" + response.getItemId()));
            }
        }
    }
}
