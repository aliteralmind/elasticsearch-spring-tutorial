package superhero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Component
@RequestMapping("/detail")
public class SuperheroDetailController {

    private static final Logger logger = LoggerFactory.getLogger(SuperheroDetailController.class);


    private final SuperheroDataService superheroDataService;

    @Autowired
    public SuperheroDetailController(SuperheroDataService superheroDataService) {
        this.superheroDataService = superheroDataService;
    }

    @RequestMapping
    public ModelAndView detail(
            @RequestParam(value = "name", required = true) String name) {
        Superhero superhero = superheroDataService.getSuperheroFromName(name);

        if (superhero == null) {
            superhero = new Superhero("UnknownSuperHero", "Unknown super hero name: \"" + name + "\"");
            //TODO: Pass this to an error page.
//            throw new UnknownSuperheroException(name);
        }

        logger.debug(superhero.toString());
        return new ModelAndView("detail", "superhero", superhero);
    }
}
