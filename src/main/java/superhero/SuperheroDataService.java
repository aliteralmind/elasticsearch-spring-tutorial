package superhero;

public interface SuperheroDataService {
    Superhero getSuperheroFromName(String name);
}
