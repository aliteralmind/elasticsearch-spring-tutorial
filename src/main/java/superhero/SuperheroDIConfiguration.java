package superhero;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = {"springes"})
public class SuperheroDIConfiguration {

    @Bean
    public SuperheroDataService getSuperheroDataService() {
        return new MemorySuperheroDataService();
    }
}
