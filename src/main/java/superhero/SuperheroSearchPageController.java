package superhero;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Component
@RequestMapping("/")
public class SuperheroSearchPageController {

    @RequestMapping
    public ModelAndView searchPage() {
        return new ModelAndView("search_page", "searchmessage", "search page goes here");
    }
}
