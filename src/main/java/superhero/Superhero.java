package superhero;

public class Superhero {

    private final String name;

    private final String summary;

    public Superhero(String name, String summary) {
        this.name = name;
        this.summary = summary;
    }

    public String getName() {
        return name;
    }

    public String getSummary() {
        return summary;
    }

    @Override
    public String toString() {
        return "Superhero{" +
               "name='" + name + '\'' +
               ", summary='" + summary + '\'' +
               '}';
    }
}
