package superhero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class MemorySuperheroDataService implements SuperheroDataService {

    private static final Logger logger = LoggerFactory.getLogger(MemorySuperheroDataService.class);

    public static final Map<String, Superhero> NAME_TO_SUPERHERO_MAP = newMap();
    private static final String TAB_DELIM_DATA_FILE_PATH = "/Users/jeff.epstein/Documents/elasticsearch/elasticsearch-spring-tutorial/src/main/resources/superhero_data.txt";

    @Override
    public Superhero getSuperheroFromName(String name) {
        return NAME_TO_SUPERHERO_MAP.get(name);
    }


    private static Map<String, Superhero> newMap() {
        final Map<String, Superhero> map = new HashMap<String, Superhero>();

        Stream<String> lineStream;
        try {
            lineStream = Files.lines(Paths.get(TAB_DELIM_DATA_FILE_PATH), Charset.defaultCharset());
        } catch (IOException e) {
            throw new RuntimeException("Cannot read " + TAB_DELIM_DATA_FILE_PATH + ": " + e);
        }

        lineStream.forEach(line -> {
            if(line.length() == 0) return;

            logger.debug(line);
            String[] pieces = line.split("\t");
            final String name = pieces[0];
            final String summary = pieces[1];
            map.put(name, new Superhero(name, summary));
        });
        return map;
    }
}
