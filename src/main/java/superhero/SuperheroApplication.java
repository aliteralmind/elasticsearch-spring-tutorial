package superhero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SuperheroApplication {

    @Bean
    public SuperheroDataService superheroDataService() {
        return new MemorySuperheroDataService();
    }

    public static void main(String[] args) {
        SpringApplication.run(SuperheroApplication.class, args);
    }
}
