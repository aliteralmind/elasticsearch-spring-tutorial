package superhero;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such Order")  // 404
public class UnknownSuperheroException extends RuntimeException {

    public UnknownSuperheroException(String name) {
        super("Unknown name: \"" + name + "\"");
    }
}
