package superhero;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class SuperheroSearchPageControllerTest {

    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setSuffix(".jsp");

        final SuperheroDataService superheroDataService = new SuperheroApplication()
                .superheroDataService();
        final SuperheroDetailController superheroDetailController = new SuperheroDetailController(
                superheroDataService);
        mvc = MockMvcBuilders.standaloneSetup(superheroDetailController)
                .setViewResolvers(viewResolver).build();
    }

    @Test
    public void getDetail() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/detail?name=Spider-Man")
                            .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(
                        "{\"name\":\"Spider-Man\",\"summary\":\"Spider-Man is a fictional character, a comic book superhero who appears in comic books published by Marvel Comics. Created by writer-editor Stan Lee and writer-artist Steve Ditko, he first appeared in Amazing Fantasy #15. Lee and Ditko conceived the character as an orphan being raised by his Aunt May and Uncle Ben, and as a teenager, having to deal with the normal struggles of adolescence in addition to those of a costumed crimefighter. Spider-Mans creators gave him super strength and agility, the ability to cling to most surfaces, shoot spider-webs using wrist-mounted devices of his own invention which he called web-shooters, and react to danger quickly with his spider-sense, enabling him to combat his foes.\\nWhen Spider-Man first appeared in the early 1960s, teenagers in superhero comic books were usually relegated to the role of sidekick to the protagonist. The Spider-Man series broke ground by featuring Peter Parker, a teenage high school student and person behind Spider-Mans secret identity to whose self-obsessions with rejection, inadequacy, and loneliness young readers could relate. Unlike previous teen heroes such as Bucky and Robin, Spider-Man did not benefit from being the protégé of any adult superhero mentors like Captain America and Batman, and thus had to learn for himself that with great power there must also come great responsibility—a line included in a text box in the final panel of the first Spider-Man story, but later retroactively attributed to his guardian, the late Uncle Ben.\"}")));
    }
}
